package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Waits.collapsed;
import static com.atlassian.upm.pageobjects.Waits.expanded;

public abstract class UPMTab
{
    protected @Inject AtlassianWebDriver driver;
    abstract protected String getPluginContainerId(TabCategory category);
    
    public void expandAllResults(TabCategory category)
    {
        By expandAllSelector = By.cssSelector(getPluginContainerId(category) + " a.upm-expand-all");
        driver.waitUntilElementIsVisible(expandAllSelector);

        WebElement expandAllLink = driver.findElement(expandAllSelector);
        expandAllLink.click();
        
        driver.waitUntil(expanded(driver.findElements(By.cssSelector(getPluginContainerId(category) + " div.upm-plugin"))));
    }

    public void collapseAllResults(TabCategory category)
    {
        By collapseAllSelector = By.cssSelector(getPluginContainerId(category) + " a.upm-collapse-all");
        driver.waitUntilElementIsVisible(collapseAllSelector);

        WebElement collapseAllLink = driver.findElement(collapseAllSelector);
        collapseAllLink.click();
        
        driver.waitUntil(collapsed(driver.findElements(By.cssSelector(getPluginContainerId(category) + " div.upm-plugin"))));
    }
    
    public interface TabCategory
    {
        String getOptionValue();
    }
}
