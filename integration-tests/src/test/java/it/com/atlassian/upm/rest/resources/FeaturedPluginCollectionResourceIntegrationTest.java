package it.com.atlassian.upm.rest.resources;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.upm.rest.representations.FeaturedPluginCollectionRepresentation;
import com.atlassian.upm.test.RestTester;
import com.atlassian.upm.test.UpmResourceTestBase;

import com.sun.jersey.api.client.ClientResponse;

import org.junit.Test;

import static com.atlassian.upm.test.JerseyClientMatchers.ok;
import static com.atlassian.upm.test.JerseyClientMatchers.unauthorized;
import static com.atlassian.upm.test.TestPlugins.CANNOT_DISABLE_MODULE;
import static com.atlassian.upm.test.TestPlugins.JIRA_CALENDAR;
import static com.atlassian.upm.test.TestPlugins.JIRA_CHART;
import static com.atlassian.upm.test.TestPlugins.NO_BINARY_URL;
import static com.atlassian.upm.test.UpmIntegrationMatchers.containsPlugins;
import static com.atlassian.upm.test.UpmTestGroups.REFAPP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FeaturedPluginCollectionResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatFeaturedPluginsResourceContainsAllPlugins()
    {
        FeaturedPluginCollectionRepresentation featuredPlugins = restTester.getFeaturedPlugins();
        assertThat(featuredPlugins.getPlugins(), containsPlugins(
            JIRA_CALENDAR,
            JIRA_CHART,
            NO_BINARY_URL,
            CANNOT_DISABLE_MODULE
        ));
    }

    @Test
    public void assertThatFeaturedPluginsResourceContainsSameAmountAsMaxResults()
    {
        FeaturedPluginCollectionRepresentation featuredPlugins = restTester.getFeaturedPlugins(1, 0);
        assertThat(featuredPlugins.getPlugins(), containsPlugins(
            JIRA_CALENDAR
        ));
    }

    @Test
    public void assertThatFeaturedPluginsResourceStartsWithSpecifiedStartIndex()
    {
        FeaturedPluginCollectionRepresentation featuredPlugins = restTester.getFeaturedPlugins(0, 1);
        assertThat(featuredPlugins.getPlugins(), containsPlugins(
            JIRA_CHART,
            NO_BINARY_URL,
            CANNOT_DISABLE_MODULE
        ));
    }

    @Test
    public void assertThatFeaturedPluginsHasSafeModeFalseWhenNotInSafeMode()
    {
        FeaturedPluginCollectionRepresentation featuredPlugins = restTester.getFeaturedPlugins();
        assertFalse(featuredPlugins.isSafeMode());
    }

    @Test
    public void assertThatFeaturedPluginsHasSafeModeTrueWhenInSafeMode()
    {
        restTester.enterSafeMode();
        try
        {
            FeaturedPluginCollectionRepresentation featuredPlugins = restTester.getFeaturedPlugins();
            assertTrue(featuredPlugins.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }

    /**
     * Refapp-specific tests for the AvailablePluginCollectionResource because other apps don't have users other than admin.
     * For example, barney and fred only exist in refapp, so this can be tested only there.
     */
    @Test
    @TestGroups(REFAPP)
    public void unauthorizedResponseForNonAdmins()
    {
        RestTester restTesterBarney = new RestTester("barney", "barney");
        try
        {
            final ClientResponse responseNonAdmin = restTesterBarney.getFeaturedPlugins(ClientResponse.class);
            assertThat(responseNonAdmin, is(unauthorized()));
        }
        finally
        {
            restTesterBarney.destroy();
        }
    }

    @Test
    public void okResponseForAdmins()
    {
        final ClientResponse responseAdmin = restTester.getFeaturedPlugins(ClientResponse.class);
        assertThat(responseAdmin, is(ok()));
    }
}
