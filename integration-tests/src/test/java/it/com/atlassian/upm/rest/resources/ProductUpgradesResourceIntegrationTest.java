package it.com.atlassian.upm.rest.resources;

import com.atlassian.upm.rest.representations.ProductUpgradesRepresentation;
import com.atlassian.upm.test.UpmResourceTestBase;

import org.junit.Test;

import static com.atlassian.upm.test.UpmIntegrationMatchers.containsVersions;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProductUpgradesResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatProductUpgradesResourceContainsAllVersions()
    {
        ProductUpgradesRepresentation productUpgradesRepresentation = restTester.getProductUpgrades();
        assertThat(productUpgradesRepresentation.getVersions(), containsVersions("4.0", "4.0.1", "4.0.2", "4.1"));
    }

    @Test
    public void assertThatProductUpgradesPluginsHasSafeModeFalseWhenNotInSafeMode()
    {
        ProductUpgradesRepresentation productUpgradesRepresentation = restTester.getProductUpgrades();
        assertFalse(productUpgradesRepresentation.isSafeMode());
    }

    @Test
    public void assertThatProductUpgradesPluginsHasSafeModeTrueWhenInSafeMode()
    {
        restTester.enterSafeMode();
        try
        {
            ProductUpgradesRepresentation productUpgradesRepresentation = restTester.getProductUpgrades();
            assertTrue(productUpgradesRepresentation.isSafeMode());
        }
        finally
        {
            restTester.exitSafeModeAndRestoreSavedConfiguration();
        }
    }
}
