package com.atlassian.upm.test;

import java.io.IOException;
import java.util.List;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.upm.UpmTab;
import com.atlassian.upm.rest.UpmUriBuilder;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import be.roam.hue.doj.Doj;

import static com.atlassian.upm.UpmTab.AUDIT_LOG_TAB;
import static com.atlassian.upm.UpmTab.COMPATIBILITY_TAB;
import static com.atlassian.upm.UpmTab.INSTALL_TAB;
import static com.atlassian.upm.UpmTab.MANAGE_EXISTING_TAB;
import static com.atlassian.upm.UpmTab.OSGI_TAB;
import static com.atlassian.upm.UpmTab.UPGRADE_TAB;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;

public class UpmUiTester implements UiTester
{
    public static final String USER_INSTALLED_GRP = "user";
    public static final String BUNDLED_GRP = "system";

    private final UpmUriBuilder uriBuilder = new UpmUriBuilder(ApplicationPropertiesImpl.getStandardApplicationProperties());
    private final UiTester productTester;

    UpmUiTester(UiTester productTester)
    {
        this.productTester = productTester;
    }

    /**
     * Sets the {@code currentPage} to the UPM page, and returns the {@code currentPage}
     *
     * @return the current UPM page
     */
    public HtmlPage goToUpmPage()
    {
        return gotoPage("plugins/servlet/upm");
    }

    /**
     * Returns the embedded build number element from the velocity file.
     *
     * @return the embedded build number element from the velocity file.
     */
    public String getBuildNumber()
    {
        return currentPage().getById("upm-product-build-number").attribute("value");
    }

    /**
     * Filters the visible plugins in the {@code currentTab}, using the {@code filterValue}
     *
     * @param currentTab the tab that is currently in question
     * @param filterValue the value to filter the plugins with
     */
    public void filterPluginList(UpmTab currentTab, String filterValue)
    {
        HtmlTextInput input = getFilterBox(currentTab);
        checkState(input != null, "Cannot find search filter input in current page");

        input.setText(filterValue.substring(0, filterValue.length() - 1));
        try
        {
            input.type(filterValue.charAt(filterValue.length() - 1));
            waitForAsyncEventsThatBeginWithinDefaultTime();
        }
        catch (IOException e)
        {
            throw new RuntimeException("Could not type last character", e);
        }
    }

    /**
     * Executes the search in the intall tab
     *
     * @param searchValue the value to search PAC
     */
    public void searchPac(String searchValue) throws IOException
    {
        HtmlTextInput input = getPacSearchBox();
        checkState(input != null, "Cannot find search input in current page");

        input.setValueAttribute(searchValue);

        input.type('\n');
        waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    /**
     * Selects the specified version in the compatibility check dropdown
     *
     * @param version the version to select
     * @throws IOException if the 'click' failed
     * @throws ElementNotFoundException if the dropdown did not contain the requested version
     */
    public void selectVersionForCompatibilityCheck(String version) throws IOException, ElementNotFoundException
    {
        HtmlSelect dropDown = (HtmlSelect) currentPage().getById("upm-compatibility-version").firstElement();
        checkState(dropDown != null, "Cannot find version compatibility dropdown in current page");

        HtmlForm form = (HtmlForm) currentPage().getById("upm-compatibility-form").firstElement();
        HtmlSubmitInput submitButton = form.getInputByName("submit");

        dropDown.getOptionByText(version).click();
        waitForAsyncEventsThatBeginWithinDefaultTime();
        submitButton.click();
        waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    /**
     * Click and go to the Upgrade tab
     */
    public void clickUpgradeTab()
    {
        clickTab(UPGRADE_TAB);
    }

    /**
     * Click and go to the Install tab
     */
    public void clickInstallTab()
    {
        clickTab(INSTALL_TAB);
    }

    /**
     * Click and go to the Manage Existing tab
     */
    public void clickManageExistingTab()
    {
        clickTab(MANAGE_EXISTING_TAB);
    }

    /**
     * Click and go to the Compatibility tab
     */
    public void clickCompatibilityTab()
    {
        clickTab(COMPATIBILITY_TAB);
    }

    /**
     * Click and go to the Audit Log tab
     */
    public void clickAuditLogTab()
    {
        clickTab(AUDIT_LOG_TAB);
    }

    public void clickOsgiTab()
    {
        clickTab(OSGI_TAB);
    }

    /**
     * Click the tab identified with {@code tabName} to go to that selected tab.
     *
     * @param tab the tab to select
     */
    public void clickTab(UpmTab tab)
    {
        clickElementWithId("upm-tab-" + tab.getName());
    }

    /**
     * Returns the label on the given tab.
     *
     * @param tab the tab
     * @return the label on the given tab.
     */
    public String getTabLabel(UpmTab tab)
    {
        return currentPage().getById("upm-tab-" + tab.getName()).text();
    }

    /**
     * Returns {@code true} if the tab identified with {@code tabName} is the currently selected tab, {@code false} otherwise
     *
     * @param tab the tab to check
     * @return {@code true} if the tab identified with {@code tabName} is the currently selected tab, {@code false} otherwise
     */
    public boolean isTabSelected(UpmTab tab)
    {
        return elementById(getTabPanelId(tab)).hasClass("upm-selected");
    }

    public boolean isUserPermittedToViewTab(UpmTab tab)
    {
        return !elementById(getTabPanelId(tab)).isEmpty();
    }

    /**
     * Click the refresh button in the Audit Log tab. Must be called from the audit log tab.
     */
    public void clickAuditLogRefreshButton()
    {
        clickElementWithId("upm-audit-log-refresh");
    }

    /**
     * Click the refresh button in the Audit Log tab. Must be called from the audit log tab.
     */
    public void clickAuditLogConfigurePurgeLink()
    {
        clickElementWithId("upm-log-configure");
    }

    /**
     * Will attempt to set the audit logs purge days in the UPM UI.
     *
     * @param newDays the value to submit.
     */
    public void updateAuditLogPurgeDays(String newDays) throws IOException
    {
        clickAuditLogTab();
        clickAuditLogConfigurePurgeLink();
        HtmlTextInput daysInput = getDaysInput();

        checkState(daysInput != null, "Cannot find the audit log purge days input");

        daysInput.setValueAttribute(newDays);

        HtmlSubmitInput submitButton = (HtmlSubmitInput) currentPage().getById("upm-log-configuration-submit").firstElement();
        submitButton.click();
        waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public HtmlTextInput getDaysInput()
    {
        return (HtmlTextInput) currentPage().getById("upm-log-configuration-days").firstElement();
    }

    public HtmlElement getMessageElement()
    {
        return currentPage().get("div.upm-message.error").firstElement();
    }

    public Matcher<? super HtmlElement> containsMessage(String expected)
    {
        final Matcher<String> messageMatcher = containsString(expected);
        return new TypeSafeDiagnosingMatcher<HtmlElement>()
        {
            @Override
            protected boolean matchesSafely(HtmlElement item, Description mismatchDescription)
            {
                String message = Iterables.find(item.getAllHtmlChildElements(), withMessageTextClass).getTextContent();
                if (!messageMatcher.matches(message))
                {
                    messageMatcher.describeMismatch(message, mismatchDescription);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendDescriptionOf(messageMatcher);
            }
        };
    }

    /**
     * Returns the corresponding tab ID identified with {@code tabName}.
     *
     * @param tab the tab
     * @return the corresponding tab ID identified with {@code tabName}
     */
    public String getTabPanelId(UpmTab tab)
    {
        return "upm-panel-" + tab.getName().toLowerCase();
    }

    public Iterable<String> getVisiblePluginsForId(String id)
    {
        List<String> allPlugins = asList(elementById(id).get("div.upm-plugin h4.upm-plugin-name").trimmedTexts());
        List<String> hiddenPlugins = asList(elementById(id).get("div.upm-plugin").withAttributeContaining("class", "hidden").get("h4.upm-plugin-name").trimmedTexts());
        return filter(allPlugins, not(in(hiddenPlugins)));
    }

    /**
     * The names of all the plugins currently visible, whether expanded or not, on the specified tab
     *
     * @param tab the tab whose plugins should be returned
     * @return the names of the visible plugins
     */
    public Iterable<String> getVisiblePlugins(UpmTab tab)
    {
        return getVisiblePluginsForId(getTabPanelId(tab));
    }

    /**
     * The names of all plugins currently visible, whether expanded or not, in the 'incompatible' category
     * on the compatibility tab
     *
     * @return the names of all plugins currently visible in the 'incompatible' category
     */
    public Iterable<String> getVisibleIncompatiblePlugins()
    {
        return getVisiblePluginsForId("upm-incompatible-plugins");
    }

    /**
     * The names of all plugins currently visible, whether expanded or not, in the 'upgrade' category
     * on the compatibility tab
     *
     * @return the names of all plugins currently visible in the 'upgrade' category
     */
    public Iterable<String> getVisibleUpgradePlugins()
    {
        return getVisiblePluginsForId("upm-need-upgrade-plugins");
    }

    /**
     * The names of all plugins currently visible, whether expanded or not, in the 'requires product upgrade' category
     * on the compatibility tab
     *
     * @return the names of all plugins currently visible in the 'requires product upgrade' category
     */
    public Iterable<String> getVisibleProductUpgradePlugins()
    {
        return getVisiblePluginsForId("upm-need-product-upgrade-plugins");
    }

    /**
     * The names of all plugins currently visible, whether expanded or not, in the 'compatible' category
     * on the compatibility tab
     *
     * @return the names of all plugins currently visible in the 'compatible' category
     */
    public Iterable<String> getVisibleCompatiblePlugins()
    {
        return getVisiblePluginsForId("upm-compatible-plugins");
    }

    /**
     * The names of all plugins currently visible, whether expanded or not, in the 'unknown' category
     * on the compatibility tab
     *
     * @return the names of all plugins currently visible in the 'unknown' category
     */
    public Iterable<String> getVisibleUnknownPlugins()
    {
        return getVisiblePluginsForId("upm-unknown-plugins");
    }

    public HtmlElement getPluginDiv(String group, TestPlugins plugin)
    {
        return elementById("upm-" + group + "-plugins").get("input").withValue(uriBuilder.buildPluginUri(plugin.getKey()).toString()).parent().firstElement();
    }

    public HtmlElement getPluginDiv(TestPlugins plugin)
    {
        return currentPage().get("input").withValue(uriBuilder.buildAvailablePluginUri(plugin.getKey()).toString()).parent().firstElement();
    }

    public String getPluginDivId(String group, TestPlugins plugin)
    {
        HtmlElement pluginDiv = (group == null) ? getPluginDiv(plugin) : getPluginDiv(group, plugin);
        if (pluginDiv == null)
        {
            return null;
        }
        return pluginDiv.getId();
    }

    public void expandPluginRow(String pluginDivId) throws IOException
    {
        checkNotNull(pluginDivId, "pluginDivId");

        Doj element = elementById(pluginDivId);
        if (!element.hasClass("expanded"))
        {
            element.get("div.upm-plugin-row").click();
            waitForAsyncEventsThatBeginWithinDefaultTime();
        }
    }

    public void clickManagePluginModulesLink(String pluginDivId) throws IOException
    {
        elementById(pluginDivId).get("a.upm-module-toggle").click();
    }

    private HtmlTextInput getFilterBox(UpmTab tab)
    {
        return (HtmlTextInput) elementById(getTabPanelId(tab)).get("input.upm-filterbox").firstElement();
    }

    private HtmlTextInput getPacSearchBox()
    {
        return (HtmlTextInput) currentPage().getById("upm-install-search-box").firstElement();
    }

    private static final Predicate<? super HtmlElement> withMessageTextClass = new Predicate<HtmlElement>()
    {
        public boolean apply(HtmlElement element)
        {
            if (element.getAttribute("class").contains("upm-message-text"))
            {
                return true;
            }
            return false;
        }
    };
    // ------------------------------------------------- delegate methods

    public void clickElementWithId(String id)
    {
        productTester.clickElementWithId(id);
    }

    public void closeWindows()
    {
        productTester.closeWindows();
    }

    public void restore(String backup)
    {
        throw new UnsupportedOperationException();
    }

    public void restore(String backup, String user)
    {
        throw new UnsupportedOperationException();
    }

    public boolean isJavaScriptEnabled()
    {
        throw new UnsupportedOperationException();
    }

    public void setJavaScriptEnabled(boolean enabled)
    {
        throw new UnsupportedOperationException();
    }

    public Doj currentPage()
    {
        return productTester.currentPage();
    }

    public void destroy()
    {
        productTester.destroy();
    }

    public Doj elementById(String id)
    {
        return productTester.elementById(id);
    }

    public String getBaseUrl()
    {
        return productTester.getBaseUrl();
    }

    public HtmlElement getElementById(String id)
    {
        return productTester.getElementById(id);
    }

    public String getHtmlContents()
    {
        return productTester.getHtmlContents();
    }

    public String getLoggedInUser()
    {
        return productTester.getLoggedInUser();
    }

    public String getStyleClass(String elementId)
    {
        return productTester.getStyleClass(elementId);
    }

    public HtmlPage gotoPage(String relativePath)
    {
        return productTester.gotoPage(relativePath);
    }

    public boolean isOnLoginPage()
    {
        return productTester.isOnLoginPage();
    }

    public void logInAs(String username)
    {
        productTester.logInAs(username);
    }

    public void logout()
    {
        productTester.logout();
    }

    public Page refreshPage()
    {
        return productTester.refreshPage();
    }

    public int waitForAsyncEventsThatBeginWithin(long delayMillis)
    {
        return productTester.waitForAsyncEventsThatBeginWithin(delayMillis);
    }

    public int waitForAsyncEventsThatBeginWithinDefaultTime()
    {
        return productTester.waitForAsyncEventsThatBeginWithinDefaultTime();
    }

    public int waitForAsyncEventsToComplete()
    {
        return productTester.waitForAsyncEventsToComplete();
    }

    public int waitForAsyncEventsToComplete(long timeoutMillis)
    {
        return productTester.waitForAsyncEventsToComplete(timeoutMillis);
    }
}
