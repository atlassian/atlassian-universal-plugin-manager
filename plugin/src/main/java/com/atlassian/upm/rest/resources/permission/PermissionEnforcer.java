package com.atlassian.upm.rest.resources.permission;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.upm.spi.Permission;
import com.atlassian.upm.spi.PermissionService;
import com.atlassian.upm.spi.Plugin;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.osgi.context.BundleContextAware;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Controls if a user has access to a given resource.
 */
public class PermissionEnforcer implements BundleContextAware, DisposableBean
{
    private final UserManager userManager;
    private final PermissionService defaultPermissionService;
    private ServiceTracker permissionServiceTracker;

    public PermissionEnforcer(@Qualifier("asyncTaskAwareUserManager") UserManager userManager,
        PermissionService defaultPermissionService)
    {
        this.userManager = checkNotNull(userManager, "userManager");
        this.defaultPermissionService = checkNotNull(defaultPermissionService, "defaultPermissionService");
    }

    public void enforcePermission(Permission permission)
    {
        if (!hasPermission(permission))
        {
            throw new PermissionDeniedException();
        }
    }

    public void enforcePermission(Permission permission, Plugin plugin)
    {
        if (!hasPermission(permission, plugin))
        {
            throw new PermissionDeniedException();
        }
    }

    public void enforcePermission(Permission permission, Plugin.Module module)
    {
        if (!hasPermission(permission, module))
        {
            throw new PermissionDeniedException();
        }
    }

    public boolean hasPermission(Permission permission)
    {
        return lookupPermissionService().hasPermission(userManager.getRemoteUsername(), permission);
    }

    public boolean hasPermission(Permission permission, Plugin plugin)
    {
        return lookupPermissionService().hasPermission(userManager.getRemoteUsername(), permission, plugin);
    }

    public boolean hasPermission(Permission permission, Plugin.Module module)
    {
        return lookupPermissionService().hasPermission(userManager.getRemoteUsername(), permission, module);
    }

    public void setBundleContext(BundleContext bundleContext)
    {
        permissionServiceTracker = new ServiceTracker(bundleContext, PermissionService.class.getName(), null);
        permissionServiceTracker.open();
    }

    public boolean isAdmin()
    {
        if (userManager.getRemoteUsername() == null)
        {
            return false;
        }
        return userManager.isAdmin(userManager.getRemoteUsername());
    }

    public void enforceAdmin()
    {
        if (!isAdmin())
        {
            throw new PermissionDeniedException();
        }
    }

    public boolean isSystemAdmin()
    {
        if (userManager.getRemoteUsername() == null)
        {
            return false;
        }
        return userManager.isSystemAdmin(userManager.getRemoteUsername());
    }

    public void enforceSystemAdmin()
    {
        if (!isSystemAdmin())
        {
            throw new PermissionDeniedException();
        }
    }

    private PermissionService lookupPermissionService()
    {
        PermissionService permissionService = (PermissionService) permissionServiceTracker.getService();
        return permissionService == null ? defaultPermissionService : permissionService;
    }

    public void destroy()
    {
        permissionServiceTracker.close();
    }
}
