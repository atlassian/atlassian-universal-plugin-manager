package com.atlassian.upm.rest.representations;

import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.atlassian.plugins.domain.model.product.Product;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.rest.UpmUriBuilder;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class ProductUpgradesRepresentation
{
    @JsonProperty private final Map<String, URI> links;
    @JsonProperty private final Collection<ProductUpgradeEntry> versions;
    @JsonProperty private final boolean safeMode;

    @JsonCreator
    public ProductUpgradesRepresentation(@JsonProperty("links") Map<String, URI> links,
        @JsonProperty("versions") Collection<ProductUpgradeEntry> versions,
        @JsonProperty("safeMode") boolean safeMode)
    {
        this.links = links;
        this.versions = versions;
        this.safeMode = safeMode;
    }

    public ProductUpgradesRepresentation(UpmUriBuilder uriBuilder, Iterable<Product> products,
        PluginAccessorAndController pluginAccessorAndController, LinkBuilder linkBuilder)
    {
        links = linkBuilder.buildLinksFor(uriBuilder.buildProductUpgradesUri()).build();
        versions = ImmutableList.copyOf(transform(products, toEntries(uriBuilder)));
        safeMode = pluginAccessorAndController.isSafeMode();
    }

    public Collection<ProductUpgradeEntry> getVersions()
    {
        return versions;
    }

    public Map<String, URI> getLinks()
    {
        return links;
    }

    public boolean isSafeMode()
    {
        return safeMode;
    }

    private Function<Product, ProductUpgradeEntry> toEntries(UpmUriBuilder uriBuilder)
    {
        return new ToEntryFunction(uriBuilder);
    }

    private final static class ToEntryFunction implements Function<Product, ProductUpgradeEntry>
    {
        private final UpmUriBuilder uriBuilder;

        public ToEntryFunction(UpmUriBuilder uriBuilder)
        {
            this.uriBuilder = uriBuilder;
        }

        public ProductUpgradeEntry apply(Product product)
        {
            return new ProductUpgradeEntry(product, uriBuilder);
        }
    }

    public static final class ProductUpgradeEntry
    {
        @JsonProperty private final String version;
        @JsonProperty private final boolean recent;
        @JsonProperty private final Map<String, URI> links;

        @JsonCreator
        public ProductUpgradeEntry(@JsonProperty("version") String version,
            @JsonProperty("recent") boolean recent,
            @JsonProperty("links") Map<String, URI> links)
        {
            this.version = checkNotNull(version, "version");
            this.recent = recent;
            this.links = ImmutableMap.copyOf(links);
        }

        public ProductUpgradeEntry(Product product, UpmUriBuilder uriBuilder)
        {
            this.version = product.getVersionNumber();
            this.recent = isRecent(product);
            this.links = ImmutableMap.of("self", uriBuilder.buildProductUpgradePluginCompatibilityUri(product.getBuildNumber()));
        }

        public URI getSelf()
        {
            return links.get("self");
        }

        public String getVersion()
        {
            return version;
        }

        public boolean isRecent()
        {
            return recent;
        }

        private static boolean isRecent(Product product)
        {
            Date releaseDate = product.getReleaseDate();
            return releaseDate == null ?
                false :
                new DateTime(releaseDate).isAfter(new DateTime().minusWeeks(2));
        }
    }
}
