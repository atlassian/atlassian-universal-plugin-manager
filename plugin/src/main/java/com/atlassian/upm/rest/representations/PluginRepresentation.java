package com.atlassian.upm.rest.representations;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.plugin.PluginRestartState;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.rest.representations.LinkBuilder.LinksMapBuilder;
import com.atlassian.upm.rest.resources.permission.PermissionEnforcer;
import com.atlassian.upm.spi.Plugin;
import com.atlassian.upm.spi.Plugin.Module;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.upm.spi.Permission.GET_PLUGIN_MODULES;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_ENABLEMENT;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_MODULE_ENABLEMENT;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_UNINSTALL;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Collections2.transform;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang.StringUtils.isNotBlank;


/**
 * Jackson representation of a particular plugin.
 */
public class PluginRepresentation
{
    private static final Logger log = LoggerFactory.getLogger(PluginRepresentation.class);
    private static final String CHANGE_REQUIRING_RESTART_REL = "change-requiring-restart";

    @JsonProperty private final Map<String, URI> links;
    @JsonProperty private final String key;
    @JsonProperty private final boolean enabled;
    @JsonProperty private final boolean enabledByDefault;
    @JsonProperty private final String version;
    @JsonProperty private final String description;
    @JsonProperty private final Vendor vendor;
    @JsonProperty private final String name;
    @JsonProperty private final Collection<ModuleEntryRepresentation> modules;
    @JsonProperty private final boolean userInstalled;
    @JsonProperty private final boolean optional;
    @JsonProperty private final boolean unrecognisedModuleTypes;
    @JsonProperty private final String configureUrl;
    @JsonProperty private final String restartState;

    @JsonCreator
    public PluginRepresentation(@JsonProperty("links") Map<String, URI> links,
        @JsonProperty("key") String key,
        @JsonProperty("enabled") boolean enabled,
        @JsonProperty("enabledByDefault") boolean enabledByDefault,
        @JsonProperty("version") String version,
        @JsonProperty("description") String description,
        @JsonProperty("vendor") Vendor vendor,
        @JsonProperty("name") String name,
        @JsonProperty("modules") Collection<ModuleEntryRepresentation> modules,
        @JsonProperty("userInstalled") boolean userInstalled,
        @JsonProperty("optional") boolean optional,
        @JsonProperty("unrecognisedModuleTypes") boolean unrecognisedModuleTypes,
        @JsonProperty("configureUrl") String configureUrl,
        @JsonProperty("restartState") String restartState)
    {
        this.links = ImmutableMap.copyOf(links);
        this.key = key;
        this.enabled = enabled;
        this.enabledByDefault = enabledByDefault;
        this.version = version;
        this.description = description;
        this.vendor = vendor;
        this.name = name;
        this.modules = ImmutableList.copyOf(modules);
        this.userInstalled = userInstalled;
        this.optional = optional;
        this.unrecognisedModuleTypes = unrecognisedModuleTypes;
        this.configureUrl = configureUrl;
        this.restartState = restartState;
    }

    PluginRepresentation(final PluginAccessorAndController pluginAccessorAndController, final Plugin plugin,
        final UpmUriBuilder uriBuilder, final LinkBuilder linkBuilder, final PermissionEnforcer permissionEnforcer)
    {
        this.key = checkNotNull(plugin, "plugin").getKey();
        this.enabled = checkNotNull(pluginAccessorAndController, "pluginAccessorAndController").isPluginEnabled(plugin.getKey());
        this.enabledByDefault = plugin.isEnabledByDefault();
        this.version = plugin.getPluginInformation().getVersion();
        this.description = plugin.getPluginInformation().getDescription();
        this.vendor = newVendor(plugin.getPluginInformation().getVendorName(), plugin.getPluginInformation().getVendorUrl());
        this.optional = pluginAccessorAndController.isOptional(plugin);
        this.userInstalled = pluginAccessorAndController.isUserInstalled(plugin);
        this.unrecognisedModuleTypes = plugin.hasUnrecognisedModuleTypes();
        this.name = plugin.getName();

        PluginRestartState restart = pluginAccessorAndController.getRestartState(plugin);
        this.restartState = RestartState.toString(restart);

        this.links = buildLinks(plugin, uriBuilder, linkBuilder, restart);

        if (permissionEnforcer.hasPermission(GET_PLUGIN_MODULES, plugin))
        {
            this.modules = transform(ImmutableList.copyOf(plugin.getModules()), new Function<Module, ModuleEntryRepresentation>()
            {
                public ModuleEntryRepresentation apply(@Nullable Module module)
                {
                    return new ModuleEntryRepresentation(module, pluginAccessorAndController, uriBuilder, linkBuilder);
                }
            });
        }
        else
        {
            this.modules = null;
        }

        this.configureUrl = getConfigureUrl(plugin);
    }

    private Map<String, URI> buildLinks(final Plugin plugin, final UpmUriBuilder uriBuilder, final LinkBuilder linkBuilder, PluginRestartState restart)
    {
        LinksMapBuilder builder = linkBuilder.buildLinkForSelf(uriBuilder.buildPluginUri(plugin.getKey()))
            .putIfPermitted(MANAGE_PLUGIN_ENABLEMENT, plugin, "modify", uriBuilder.buildPluginUri(plugin.getKey()))
            .putIfPermitted(MANAGE_PLUGIN_UNINSTALL, plugin, "delete", uriBuilder.buildPluginUri(plugin.getKey()));

        builder.put("details-link", uriBuilder.buildPacPluginDetailsLinkUri(plugin.getKey(), plugin.getVersion()));
        if (!PluginRestartState.NONE.equals(restart))
        {
            builder.put(PluginRepresentation.CHANGE_REQUIRING_RESTART_REL, uriBuilder.buildChangeRequiringRestart(plugin.getKey()));
        }

        return builder.build();
    }

    private String getConfigureUrl(final Plugin plugin)
    {
        Map<String, String> map = plugin.getPluginInformation().getParameters();

        if (isNotBlank(map.get("configure.url")))
        {
            return map.get("configure.url");
        }
        else
        {
            return "";
        }
    }

    private Vendor newVendor(String name, String url)
    {
        if (isEmpty(name))
        {
            return null;
        }

        if (isEmpty(url))
        {
            return new Vendor(name, null);
        }

        try
        {
            URI vendorUri = URI.create(url);
            return new Vendor(name, vendorUri);
        }
        catch (IllegalArgumentException iae)
        {
            // ignore it since it's not valid anyway.
            return new Vendor(name, null);
        }
    }

    /**
     * Get the URI to this representation.
     *
     * @return the representation's URI
     */
    public URI getSelfLink()
    {
        return links.get("self");
    }

    /**
     * Get the enablement status of the plugin represented.
     *
     * @return the true if the plugin is enabled, false otherwise
     */
    public boolean isEnabled()
    {
        return enabled;
    }

    /**
     * Get the enablement (by default) status of the plugin represented.
     *
     * @return the true if the plugin is enabled by default, false otherwise
     */
    public boolean isEnabledByDefault()
    {
        return enabledByDefault;
    }

    /**
     * Returns {@code true} if plugin is user-installed, {@code false} otherwise
     *
     * @return {@code true} if plugin is user-installed, {@code false} otherwise
     */
    public boolean isUserInstalled()
    {
        return userInstalled;
    }

    /**
     * @return {@code true} if plugin is considered to not be required by the host application parameter, {@code false} otherwise
     */
    public boolean isOptional()
    {
        return optional;
    }

    /**
     * Returns {@code true} if one or more of this plugin's module desciptors is of an unrecognised module type. false if not.
     *
     * @return {@code true} if one or more of this plugin's module desciptors is of an unrecognised module type. false if not.
     */
    public boolean hasUnrecognisedModuleTypes()
    {
        return unrecognisedModuleTypes;
    }

    /**
     * Get the plugin version
     *
     * @return the plugin version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * Returns {@code true} if plugin has a configure link, {@code false} otherwise
     *
     * @return {@code true} if plugin has a configure link, {@code false} otherwise
     */

    @JsonIgnore
    public boolean isConfigurable()
    {
        return StringUtils.isNotBlank(configureUrl);
    }

    public String getDescription()
    {
        return description;
    }

    public Collection<ModuleEntryRepresentation> getModules()
    {
        return modules;
    }

    public String getName()
    {
        return name;
    }

    public Vendor getVendor()
    {
        return vendor;
    }

    public String getRestartState()
    {
        return restartState;
    }

    public URI getChangeRequiringRestartLink()
    {
        return links.get(CHANGE_REQUIRING_RESTART_REL);
    }

    public static class ModuleEntryRepresentation
    {
        @JsonProperty private final String key;
        @JsonProperty private final String completeKey;
        @JsonProperty private final Map<String, URI> links;
        @JsonProperty private final boolean enabled;
        @JsonProperty private final boolean optional;
        @JsonProperty private final String name;
        @JsonProperty private final String description;
        @JsonProperty private final boolean recognisableType;

        @JsonCreator
        public ModuleEntryRepresentation(@JsonProperty("key") String key,
            @JsonProperty("completeKey") String completeKey,
            @JsonProperty("links") Map<String, URI> links,
            @JsonProperty("enabled") boolean enabled,
            @JsonProperty("optional") boolean optional,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("recognisableType") boolean recognisableType)
        {
            this.key = key;
            this.completeKey = completeKey;
            this.links = ImmutableMap.copyOf(links);
            this.name = name;
            this.enabled = enabled;
            this.description = description;
            this.optional = optional;
            this.recognisableType = recognisableType;
        }

        public ModuleEntryRepresentation(Module module,
            PluginAccessorAndController pluginAccessorAndController,
            UpmUriBuilder uriBuilder, LinkBuilder linkBuilder)
        {
            this.key = module.getKey();
            this.completeKey = module.getCompleteKey();
            this.links = linkBuilder.builder()
                .putIfPermitted(MANAGE_PLUGIN_MODULE_ENABLEMENT, module, "self", uriBuilder.buildPluginModuleUri(module.getPlugin().getKey(), key))
                .build();
            this.enabled = pluginAccessorAndController.isPluginModuleEnabled(module.getCompleteKey());
            this.optional = pluginAccessorAndController.isOptional(module);
            this.recognisableType = module.hasRecognisableType();

            // added to fix UPM-925: plugins waiting to be installed on restart aren't really all there yet, and thus
            // can have problems getting their module names and descriptions, for example if they are i18n'd
            if (pluginAccessorAndController.getRestartRequiredChange(module.getPlugin()) == null
                || !"install".equals(pluginAccessorAndController.getRestartRequiredChange(module.getPluginKey()).getAction()))
            {
                this.name = module.getName();
                this.description = module.getDescription();
            }
            else
            {
                this.name = null;
                this.description = null;
            }
        }

        public String getKey()
        {
            return key;
        }

        public String getCompleteKey()
        {
            return completeKey;
        }

        public URI getSelfLink()
        {
            return links.get("self");
        }

        public boolean isEnabled()
        {
            return enabled;
        }

        public boolean hasRecognisableType()
        {
            return recognisableType;
        }

        public String getName()
        {
            return name;
        }

        public String getDescription()
        {
            return description;
        }

        public boolean isOptional()
        {
            return optional;
        }
    }

    public static final class Vendor
    {
        @JsonProperty private final String name;
        @JsonProperty private final URI link;

        @JsonCreator
        public Vendor(@JsonProperty("name") String name, @JsonProperty("link") URI link)
        {
            this.name = checkNotNull(name, "name");
            this.link = link;
        }

        public String getName()
        {
            return name;
        }

        public URI getLink()
        {
            return link;
        }
    }
}
