package com.atlassian.upm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.jar.JarFile;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.upm.Options.Option;
import com.atlassian.upm.osgi.Version;
import com.atlassian.upm.osgi.impl.VersionImpl;
import com.atlassian.upm.rest.UpmUriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.upm.Either.left;
import static com.atlassian.upm.Either.right;
import static com.atlassian.upm.Options.none;
import static com.atlassian.upm.Options.option;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.io.File.createTempFile;
import static org.apache.commons.io.IOUtils.closeQuietly;
import static org.apache.commons.io.IOUtils.copy;

/**
 * Encapsulates the mechanism that lets UPM upgrade itself using a second temporary plugin.
 * <p>
 * This works as follows:  1. We temporarily install a "self-upgrade plugin", which has been
 * built in a separate module and embedded within the main plugin's jar.  2. We tell the self-upgrade
 * plugin the location of the UPM jar that's to be installed.  3. The self-upgrade plugin gives us a
 * REST URI which we return to the front end.  4. The front end does a POST to this URI, triggering
 * the self-upgrade plugin to complete the installation of the previously configured jar.  5. When
 * complete, the front end calls the main UPM plugin again (in the newly installed version) and it
 * uninstalls the self-upgrade plugin.
 */
public class SelfUpgradeController
{
    private static final Logger logger = LoggerFactory.getLogger(SelfUpgradeController.class);

    private static final String SELFUPGRADE_PLUGIN_JAR_RESOURCE = "atlassian-universal-plugin-manager-selfupgrade-plugin.jar";
    private static final String SELFUPGRADE_PLUGIN_KEY = "com.atlassian.upm.atlassian-universal-plugin-manager-selfupgrade-plugin";
    
    private final PluginAccessorAndController pluginAccessorAndController;
    private final SelfUpgradePluginAccessor selfUpgradePluginAccessor;
    private final UpmUriBuilder uriBuilder;
    
    public SelfUpgradeController(PluginAccessorAndController pluginAccessorAndController,
                                 SelfUpgradePluginAccessor selfUpgradePluginAccessor,
                                 UpmUriBuilder uriBuilder)
    {
        this.pluginAccessorAndController = checkNotNull(pluginAccessorAndController, "pluginAccessorAndController");
        this.selfUpgradePluginAccessor = checkNotNull(selfUpgradePluginAccessor, "selfUpgradePluginAccessor");
        this.uriBuilder = checkNotNull(uriBuilder, "uriBuilder");
    }
    
    public boolean isUpmPlugin(File plugin)
    {
        String pluginKey = getBundleAttribute(plugin, "Bundle-SymbolicName").getOrElse("");
        return pluginAccessorAndController.getUpmPluginKey().equals(pluginKey);
    }
    
    /**
     * Prepares but does not execute the self-upgrade.
     * @param upmJar  the new UPM jar that will be installed
     * @return  an {@link Either} containing either a REST URI which can be POSTed to to trigger
     *   completion of the upgrade, or, if preparation was unsuccessful, an error string.
     */
    public Either<String, URI> prepareSelfUpgrade(File upmJar)
    {
        // Is the version of the new jar lower than the current version?  If so, we should stop
        // right now-- the plugin framework will refuse to do a downgrade anyway, but we can avoid
        // the unnecessary step of installing the self-upgrade plugin.
        String newVersionStr = getBundleAttribute(upmJar, "Bundle-Version").getOrElse("").trim();
        if (newVersionStr.equals(""))
        {
            logger.warn("Could not get version string from jar");
            return left("upm.plugin.error.unexpected.error");
        }
        Version newVersion = VersionImpl.fromString(newVersionStr);
        if (newVersion.compareTo(pluginAccessorAndController.getUpmVersion()) < 0)
        {
            return left("upm.upgrade.error.downgrade");
        }
        
        // Extract the self-upgrade plugin to its own jar file
        File selfUpgradeJar;
        InputStream fromJarStream = null;
        OutputStream toJarStream = null;
        try
        {
            selfUpgradeJar = createTempFile("upm-selfupgrade", ".jar");
            fromJarStream = getClass().getClassLoader().getResourceAsStream(SELFUPGRADE_PLUGIN_JAR_RESOURCE);
            toJarStream = new FileOutputStream(selfUpgradeJar);
            copy(fromJarStream, toJarStream);
            toJarStream.close();
            logger.info("Extracted self-upgrade plugin to " + selfUpgradeJar.getAbsolutePath());
        }
        catch (IOException e)
        {
            logger.warn("Unable to extract self-upgrade plugin: " + e);
            return left("upm.plugin.error.unexpected.error");
        }
        finally
        {
            closeQuietly(fromJarStream);
            closeQuietly(toJarStream);
        }

        logger.info("Installing self-upgrade plugin");
        try
        {
            String pluginKey = pluginAccessorAndController.installPlugin(new JarPluginArtifact(selfUpgradeJar));
            if (!pluginKey.equals(SELFUPGRADE_PLUGIN_KEY))
            {
                logger.warn("Self-upgrade plugin had incorrect key \"" + pluginKey + "\"; not upgrading");
                return left("upm.plugin.error.unexpected.error");
            }
        }
        catch (Exception e)
        {
            logger.warn("Unable to install self-upgrade plugin: " + e, e);
            return left("upm.plugin.error.unexpected.error");
        }
        
        
        // Store configuration properties that the self-upgrade plugin will need, and return the
        // URI of the self-upgrade plugin's REST resource for executing the upgrade.
        String upmPluginKey = pluginAccessorAndController.getUpmPluginKey();
        URI pluginUriWillBe = uriBuilder.makeAbsolute(uriBuilder.buildPluginUri(upmPluginKey));
        URI selfUpgradePluginUri = uriBuilder.makeAbsolute(uriBuilder.buildPluginUri(SELFUPGRADE_PLUGIN_KEY));
        URI upgradeUri = selfUpgradePluginAccessor.prepareUpgrade(upmJar, upmPluginKey, pluginUriWillBe, selfUpgradePluginUri);

        return right(upgradeUri);
    }
    
    private Option<String> getBundleAttribute(File plugin, String attributeName)
    {
        try
        {
            JarFile jar = new JarFile(plugin);
            return option(jar.getManifest().getMainAttributes().getValue(attributeName));
        }
        catch (IOException e)
        {
            return none();
        }
    }
}
