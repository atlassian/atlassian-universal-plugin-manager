package com.atlassian.upm.selfupgrade.rest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.upm.selfupgrade.Constants;
import com.atlassian.upm.selfupgrade.async.SimpleAsyncTaskManager;
import com.atlassian.upm.selfupgrade.async.SimpleAsynchronousTask;
import com.atlassian.upm.selfupgrade.rest.representations.TaskRepresentation;

import static com.atlassian.upm.selfupgrade.rest.representations.MediaTypes.PENDING_TASK_JSON;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.status;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Path(Constants.REST_TASK_URI_PATH)
public class SimpleAsynchronousTaskResource
{
    private final SimpleAsyncTaskManager simpleAsyncTaskManager;
    
    public SimpleAsynchronousTaskResource(SimpleAsyncTaskManager simpleAsyncTaskManager)
    {
        this.simpleAsyncTaskManager = checkNotNull(simpleAsyncTaskManager, "simpleAsyncTaskManager");
    }
    
    @GET
    @Produces(PENDING_TASK_JSON)
    public Response getTask()
    {
        SimpleAsynchronousTask task = simpleAsyncTaskManager.getTask();
        if (task == null)
        {
            return status(NOT_FOUND).build();
        }
        TaskRepresentation rep = task.getRepresentation();
        return Response.status(rep.getStatus().getStatusCode())
            .entity(rep)
            .type(rep.getContentType()).build();
    }
}
