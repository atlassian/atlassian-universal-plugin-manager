package com.atlassian.upm.selfupgrade;

public abstract class Constants
{
    public static final String REST_BASE_PATH = "/rest/plugins/self-upgrade/1.0";
    
    public static final String REST_TASK_URI_PATH = "/tasks/default";

    // The following constants must be kept in sync with the main UPM plugin - see
    // com.atlassian.upm.SelfUpgradePluginAccessorImpl.
    public static final String SELFUPGRADE_SETTINGS_BASE = "com.atlassian.upm:selfupgrade";
    public static final String SELFUPGRADE_SETTINGS_JAR_PATH = SELFUPGRADE_SETTINGS_BASE + ".jar"; 
    public static final String SELFUPGRADE_SETTINGS_UPM_KEY = SELFUPGRADE_SETTINGS_BASE + ".key"; 
    public static final String SELFUPGRADE_SETTINGS_UPM_URI = SELFUPGRADE_SETTINGS_BASE + ".upm.uri"; 
    public static final String SELFUPGRADE_SETTINGS_SELFUPGRADE_PLUGIN_URI = SELFUPGRADE_SETTINGS_BASE + ".stub.uri"; 
}
